var CROSS = 'X';
var ZERO = 'O';
var EMPTY = ' ';

let fieldMatrix = [];
let currentPlayer = CROSS;
let fieldSize;
let GAMEOVER = false;
let isAIOn = false;

startGame();

function startGame() {
  fieldSize = Number(prompt("Введи размер поля"));
  if (!Number.isInteger(fieldSize)) fieldSize = 3;
  renderGrid(fieldSize);
  for (let i = 0; i < fieldSize; i++) {
    fieldMatrix.push(new Array(fieldSize).fill(EMPTY));
  }
  isAIOn = confirm("Хотите сыграть с ИИ?");
}

/* обработчик нажатия на клетку */
function cellClickHandler(row, col) {
  if (fieldMatrix[row][col] === EMPTY && !GAMEOVER) {
    fieldMatrix[row][col] = currentPlayer;
    renderSymbolInCell(currentPlayer, row, col);

    checkWin(fieldMatrix);

    if (currentPlayer === CROSS) currentPlayer = ZERO;
    else currentPlayer = CROSS;

    if (isAIOn && !GAMEOVER) AIplay();
  }

  console.log(`Clicked on cell: ${row}, ${col}`);
}

function AIplay() {
  let isEmptyCell = false;
  let row, col;
  while (!isEmptyCell) {
    row = getRandomInt(fieldSize);
    col = getRandomInt(fieldSize);
    if (fieldMatrix[row][col] === EMPTY) isEmptyCell = true;
  }
  fieldMatrix[row][col] = currentPlayer;
  renderSymbolInCell(currentPlayer, row, col);
  checkWin(fieldMatrix);
  if (currentPlayer === CROSS) currentPlayer = ZERO;
  else currentPlayer = CROSS;
}

function getRandomInt(max) {
  return Math.floor(Math.random() * Math.floor(max));
}

// проверяем наступила ли победа
function checkWin(fieldMatrix) {
  let isHaveEmpty = false;
  // Победа по горизонтале
  for (let row = 0; row < fieldSize; row++) {
    for (let col = 0; col < fieldSize; col++) {
      if (fieldMatrix[row][col] !== currentPlayer) break;
      if (fieldSize - 1 === col) {
        for (let i = 0; i < fieldSize; i++) {
          renderSymbolInCell(currentPlayer, row, i, '#FF0000');
        }
        showWinnerMessage(currentPlayer);
        GAMEOVER = true;
      }
    }
  }
  // Победа по вертикали
  for (let col = 0; col < fieldSize; col++) {
    for (let row = 0; row < fieldSize; row++) {
      if (fieldMatrix[row][col] !== currentPlayer) break;
      if (fieldSize - 1 === row) {
        for (let i = 0; i < fieldSize; i++) {
          renderSymbolInCell(currentPlayer, i, col, '#FF0000');
        }
        showWinnerMessage(currentPlayer);
        GAMEOVER = true;
      }
    }
  }
  // Главная вертикаль
  for (let rowcol = 0; rowcol < fieldSize; rowcol++) {
    if (fieldMatrix[rowcol][rowcol] !== currentPlayer) break;
    if (rowcol === fieldSize - 1) {
      for (let i = 0; i < fieldSize; i++) {
        renderSymbolInCell(currentPlayer, i, i, '#FF0000');
      }
      showWinnerMessage(currentPlayer);
      GAMEOVER = true;
    }
  }
  // Побочная вертикаль
  for (let row = 0, col = fieldSize - 1; row < fieldSize; row++, col--) {
    if (fieldMatrix[row][col] !== currentPlayer) break;
    if (row === fieldSize - 1) {
      for (let i = fieldSize - 1, j = 0; j < fieldSize; i--, j++) {
        renderSymbolInCell(currentPlayer, i, j, '#FF0000');
      }
      showWinnerMessage(currentPlayer);
      GAMEOVER = true;
    }
  }
  // Ничья
  if (!GAMEOVER) {
    for (let row = 0; row < fieldSize; row++) {
      for (let col = 0; col < fieldSize; col++) {
        if (fieldMatrix[row][col] === EMPTY) isHaveEmpty = true;
      }
    }
    if (isHaveEmpty === false) {
      GAMEOVER = true;
      showMessage("Победила дружба!");
    }
  }
}

function showWinnerMessage(winner) {
  if (winner === CROSS) showMessage("Победили крестики!");
  else showMessage("Победили нолики!");
}

/* обработчик нажатия на кнопку "Сначала" */
function resetClickHandler() {
  for (let row = 0; row < fieldSize; row++) {
    for (let col = 0; col < fieldSize; col++) {
      fieldMatrix[row][col] = EMPTY;
      renderSymbolInCell(EMPTY, row, col);
    }
  }
  showMessage('');
  GAMEOVER = false;
  currentPlayer = CROSS;
  console.log('reset!');
}


/* Служебные фукнции для взаимодействия с DOM. Данные функции нельзя редактировать! */
/* Показать сообщение */
function showMessage(text) {
  var msg = document.querySelector('.message');
  msg.innerText = text
}

/* Нарисовать игровое поле заданного размера */
function renderGrid(dimension) {
  var container = getContainer();
  container.innerHTML = '';

  for (let i = 0; i < dimension; i++) {
    var row = document.createElement('tr');
    for (let j = 0; j < dimension; j++) {
      var cell = document.createElement('td');
      cell.textContent = EMPTY;
      cell.addEventListener('click', () => cellClickHandler(i, j));
      row.appendChild(cell);
    }
    container.appendChild(row);
  }
}

/* Нарисовать символ symbol в ячейку(row, col) с цветом color */
function renderSymbolInCell(symbol, row, col, color = '#333') {
  var targetCell = findCell(row, col);

  targetCell.textContent = symbol;
  targetCell.style.color = color;
}

function findCell(row, col) {
  var container = getContainer();
  var targetRow = container.querySelectorAll('tr')[row];
  return targetRow.querySelectorAll('td')[col];
}

function getContainer() {
  return document.getElementById('fieldWrapper');
}

/* Test Function */
/* Победа первого игрока */
function testWin() {
  clickOnCell(0, 2);
  clickOnCell(0, 0);
  clickOnCell(2, 0);
  clickOnCell(1, 1);
  clickOnCell(2, 2);
  clickOnCell(1, 2);
  clickOnCell(2, 1);
}

/* Ничья */
function testDraw() {
  clickOnCell(2, 0);
  clickOnCell(1, 0);
  clickOnCell(1, 1);
  clickOnCell(0, 0);
  clickOnCell(1, 2);
  clickOnCell(1, 2);
  clickOnCell(0, 2);
  clickOnCell(0, 1);
  clickOnCell(2, 1);
  clickOnCell(2, 2);
}

function clickOnCell(row, col) {
  findCell(row, col).click();
}

/* вывести в консоль матрицу */
function outputArray(array) {
  array.forEach(element => {
    console.log(element);
  });
}